Rails.application.routes.draw do
  get 'orders/paid'
  get 'orders/making'
  get 'orders/shipped'
  get 'orders/completed'
  get 'orders/cancelled'
  
  
  get 'supplier_orders', to: 'supplier_orders#index'
  
  get 'supplier_orders/show'

  get 'supplier_orders/edit'

  get 'supplier_orders/making'
  get 'supplier_orders/shipped'
  get 'supplier_orders/completed'
 
  resources :orders
  root to: 'visitors#index'
  devise_for :users
  resources :users
end
