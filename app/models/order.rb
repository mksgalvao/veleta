class Order < ApplicationRecord
  enum status: [:ordered, :paid, :making, :shipped, :completed, :cancelled]
  after_initialize :set_default_status, :if => :new_record?

  def set_default_status
    self.status ||= :ordered
  end
end
