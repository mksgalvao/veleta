class SupplierOrdersController < ApplicationController
  before_filter :authenticate_user!
  before_action :supplier_only
  before_action :set_order, only: [:show, :paid, :making, :shipped, :completed, :cencelled]
  
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.where(status:[:paid, :making, :shipped, :cancelled])
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end
  
  def making
    @order.status = :making
    @order.save
    respond_to do |format|
      format.html { redirect_to supplier_orders_url, notice: 'Pedido em produção' }
      format.json { head :no_content }
    end
  end
  
  def shipped
    @order.status = :shipped
    @order.save
    respond_to do |format|
      format.html { redirect_to supplier_orders_url, notice: 'Pedido enviado!' }
      format.json { head :no_content }
    end
  end
  
  def completed
    @order.status = :completed
    @order.save
    respond_to do |format|
      format.html { redirect_to supplier_orders_url, notice: 'Pedido concluído' }
      format.json { head :no_content }
    end
  end

  private
  
    def supplier_only
      unless current_user.supplier?
        redirect_to root_path, :alert => "Acesso negado!"
      end
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:status, :order_date, :client_name, :email, :phone, :model, :color, :size, :hood_color, :sleeves_color, :embroidery_type, :main_embroidery, :secondary_embroidery, :embroidery_color, :manufactured_date, :delivery_date, :price, :cost_price)
    end
end
