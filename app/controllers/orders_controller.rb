class OrdersController < ApplicationController
  before_filter :authenticate_user!
  before_action :admin_only
  before_action :set_order, only: [:show, :paid, :making, :shipped, :completed, :cencelled, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def paid
    @order.status = :paid
    @order.save
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido encaminhado para a fábrica!' }
      format.json { head :no_content }
    end
  end
  
  def making
    @order.status = :making
    @order.save
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido em produção' }
      format.json { head :no_content }
    end
  end
  
  def shipped
    @order.status = :shipped
    @order.save
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido enviado!' }
      format.json { head :no_content }
    end
  end
  
  def completed
    @order.status = :completed
    @order.save
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido concluído' }
      format.json { head :no_content }
    end
  end
  
   def cancelled
    @order.status = :cencelled
    @order.save
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Pedido cancelado!' }
      format.json { head :no_content }
    end
  end
  
  
  private
  
    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Acesso negado!"
      end
    end
    
     # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end
   
    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:status, :order_date, :client_name, :email, :phone, :model, :color, :size, :hood_color, :sleeves_color, :embroidery_type, :main_embroidery, :secondary_embroidery, :embroidery_color, :manufactured_date, :delivery_date, :price, :cost_price)
    end
end
