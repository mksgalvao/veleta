class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :status
      t.datetime :order_date
      t.string :client_name
      t.string :email
      t.string :phone
      t.integer :model
      t.string :color
      t.integer :size
      t.string :hood_color
      t.string :sleeves_color
      t.integer :embroidery_type
      t.string :main_embroidery
      t.string :secondary_embroidery
      t.string :embroidery_color
      t.date :manufactured_date
      t.date :delivery_date
      t.decimal :price
      t.decimal :cost_price

      t.timestamps
    end
  end
end
