FactoryGirl.define do
  factory :order do
    status 1
    order_date "2016-12-05 00:37:48"
    client_name "MyString"
    email "MyString"
    phone "MyString"
    model 1
    color "MyString"
    size 1
    hood_color "MyString"
    sleeves_color "MyString"
    embroidery_type 1
    main_embroidery "MyString"
    secondary_embroidery "MyString"
    embroidery_color "MyString"
    manufactured_date "2016-12-05"
    delivery_date "2016-12-05"
    price "9.99"
    cost_price "9.99"
  end
end
